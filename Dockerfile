####################################################################
#
#
#
#
#
#
#
####################################################################

FROM		ehudkaldor/alpine-s6
# FROM    alpine

LABEL 	maintainer="Ehud Kaldor <ehud@unfairfunction.org>"

ENV     WEBSITE www.duckduckgo.com
ENV     USER bladerunner
COPY    install_scripts /install_scripts


RUN \
  for installer in $(ls /install_scripts); do \
  EXIT_CODE=0 ; \
  if [ -f /install_scripts/$installer ]; then \
  printf "\n\n\n================   running $installer  ===================\n\n" ; \
  /install_scripts/$installer ; \
  EXIT_CODE=$? ; \
  if [ 0 -ne $EXIT_CODE ]; then \
  printf "\n\n\n~~~~~~~~~~~~~~[ ERROR ] Errors running $installer\n\n\n" &&  \
  exit $EXIT_CODE; \
  fi \
  fi ; \
  done && \
  printf "================   clean up  ===================\n" && \
  rm -rf /root/.ssh && \
  rm -rf $BUILD_DIR && \
  rm -rf /install_scripts

COPY  chrome.json /home/$USER/chrome.json
COPY  /rootfs /

ENTRYPOINT	["/init"]
