# docker-chromium
A dockerized image of Chromium browser

adding `-e WEBSITE="<website url>"` to `docker run` will start the browser at <website url>

The only *must* tags are the `DISPLAY` and `/tmp/.X11-unix`, as you can see in the _minimal runner_ below.

minimal runner:

`docker run \
  -d \
  --rm \
  -e DISPLAY=unix$DISPLAY \
  -v /tmp/.X11-unix:/tmp/.X11-unix \
  --name $url \
  ehudkaldor/chromium:latest`


more available tags:

`docker run \
  -d \
  --rm \
  --device /dev/snd \
  --device /dev/dri \
	--device /dev/video0 \
	--group-add audio \
	--group-add video \
  -e WEBSITE="https://nvidia.com" \
	-e DISPLAY=unix$DISPLAY \
  -e GDK_SCALE \
  -e GDK_DPI_SCALE \
	-v /tmp/.X11-unix:/tmp/.X11-unix \
	-v /etc/localtime:/etc/localtime:ro \

	--name docker-chromium ehudkaldor/chromium:latest`
